var store = Redux.createStore(combineReducer);
var valueEl = document.getElementById('value');
var sumEl = document.getElementById('result');

function render() {
    valueEl.innerHTML = store.getState().count;
    sumEl.innerHTML = store.getState().sum;
}
render();
store.subscribe(render);

document.getElementById('decrement').addEventListener('click', function() {
    store.dispatch(decrease());
})

document.getElementById('incrementAsync').addEventListener('click', function() {
    setTimeout(function() {
        store.dispatch(increment());
    }, 1000);
});

document.getElementById('sum').addEventListener('click', function() {
    var firstValue = document.getElementById('a').value;
    var seccondValue = document.getElementById('b').value;
    store.dispatch(getSume(firstValue, seccondValue))
});