var _fakeServerApi = {
    increaseCount: function(currentCount, callback) {
        setTimeout(function() {
            callback(currentCount + 1);
        }, 5000);
    }
}