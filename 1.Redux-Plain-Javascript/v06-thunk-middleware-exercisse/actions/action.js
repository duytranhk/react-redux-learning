var decrease = function() {
    return {
        type: 'DECREMENT'
    };
}

var increment = function() {
    return {
        type: 'INCREMENT'
    }
}

var getSume = function(a, b) {
    return {
        type: 'SUM',
        a: a,
        b: b
    }
}


var asyncIncrease = function(dispatch, state) {
    dispatch({ type: "INCREMENT_LOADING" });
    _fakeServerApi.increaseCount(state.count.result,
        function(data) {
            dispatch({ type: 'INCREMENT' });
        })
}

var getRandomImage = function(dispatch, state) {
    var apiUrl = 'https://api.imgur.com/3/gallery/random/random/1';
    $.getJSON(apiUrl).done(function(data) {
        dispatch({
            type: 'GET_IMAGES',
            result: data.data
        });
    });
}