var store = Redux.createStore(combineReducer, Redux.applyMiddleware(logger, crashReporter, thunk));

function render() {
    var state = store.getState()
    document.getElementById('value').innerHTML = state.count.result;
    document.getElementById('result').innerHTML = state.sum;
    if (state.count.loading) {
        document.getElementById('status').innerHTML = 'is loading...';
    } else {
        document.getElementById('status').innerHTML = 'loaded';
    }

    if (state.imagesList.data) {
        var innerHTML = "";
        $.each(state.imagesList.data, function(key, value) {
            innerHTML += '<img src="' + state.imagesList.data[key].link + '"/>'
        });
        document.getElementById('imagesList').innerHTML = innerHTML;
    }
}

store.subscribe(render);
render();