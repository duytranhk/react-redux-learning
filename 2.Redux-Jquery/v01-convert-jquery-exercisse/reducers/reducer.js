// Reducer
function combineReducer(currentState, action) {
    var nextState = Object.assign({}, currentState);
    nextState = {
        count: counter(nextState.count, action),
        sum: sum(nextState.sum, action),
        imagesList: randomImages(nextState.imagesList, action)
    }

    return nextState;
}