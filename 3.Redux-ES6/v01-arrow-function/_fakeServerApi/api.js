var _fakeServerApi = {
    increaseCount: (currentCount, callback) => {
        setTimeout(() => callback(currentCount + 1), 1000);
    }
}