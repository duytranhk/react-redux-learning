var decrease = () => ({
    type: 'DECREMENT'
});

var increment = () => ({
    type: 'INCREMENT'
})

var getSume = (a, b) => ({
    type: 'SUM',
    a: a,
    b: b
})


var asyncIncrease = (dispatch, state) => {
    dispatch({ type: "INCREMENT_LOADING" });
    _fakeServerApi.increaseCount(state.count.result,
        data => dispatch({ type: 'INCREMENT' })
    );
}

var getRandomImage = (dispatch, state) => {
    var apiUrl = 'https://api.imgur.com/3/gallery/random/random/1';
    $.getJSON(apiUrl).done(data =>
        dispatch({
            type: 'GET_IMAGES',
            result: data.data
        })
    );
}