var store = Redux.createStore(combineReducer, Redux.applyMiddleware(logger, crashReporter, thunk));

function render() {
    var state = store.getState()
    $('#value').text(state.count.result);
    $('#result').text(state.sum);
    if (state.count.loading) {
        $('#status').text('is loading...');
    } else {
        $('#status').text('loaded');
    }

    if (state.imagesList.data) {
        var innerHTML = "";
        $.each(state.imagesList.data, function(key, value) {
            innerHTML += '<img src="' + state.imagesList.data[key].link + '"/>'
        });
        $('#imagesList').html(innerHTML);
    }
}

store.subscribe(render);
render();