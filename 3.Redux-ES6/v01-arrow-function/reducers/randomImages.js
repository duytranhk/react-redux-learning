var randomImages = (currentState, action) => {
    var DEFAULT_STATE = {
        data: [],
        loading: "a"
    };

    var nextState = Object.assign({}, currentState);
    if (currentState === undefined) {
        nextState = DEFAULT_STATE
        return nextState;
    }

    switch (action.type) {
        case 'GET_IMAGES':
            nextState.data = action.result;
            return nextState;
        default:
            nextState = currentState;
            return nextState;
    }
}